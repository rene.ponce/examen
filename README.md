# Instrucciones

Examen Intelligential

## Algoritmo Caracol y ejecutar el siguiente comando

Entrar al directorio

```bash
node caracol.js
```

## Contributing
Dentro del directorio Tienda en línea se encuentran diagrama de flujo, diagrama de arquitectura y diagrama entidad relación

## Biblioteca

Dentro del directorio se encuentran los proyectos administrador-web, bibliotecario-web, lector-web y backend-server

Los proyectos web se encuentran desarrollados en ReactJS y el backend server está en NodeJS

Instrucciones para levantar los ambientes

## Administrador
Entrar al directorio administrador-web que se encuentra dentro del directorio Biblioteca y ejecutar el siguiente comando

```bash
npm install
```
Una vez terminado de instalar las dependencias ejecutar el siguiente comando
```bash
npm start
```

## Bibliotecario
Entrar al directorio bibliotecario-web que se encuentra dentro del directorio Biblioteca y ejecutar el siguiente comando

```bash
npm install
```
Una vez terminado de instalar las dependencias ejecutar el siguiente comando
```bash
npm start
```

## Lector
Entrar al directorio lector-web que se encuentra dentro del directorio Biblioteca y ejecutar el siguiente comando

```bash
npm install
```
Una vez terminado de instalar las dependencias ejecutar el siguiente comando
```bash
npm start
```
## Backend
Entrar al directorio backend-server que se encuentra dentro del directorio Biblioteca y ejecutar el siguiente comando

```bash
npm install
```
Renombrar el archivo .env.example por .env

El proyecto de backend-server utiliza docker para tener un contenedor con PostgreSQL, una vez instaladas las dependencias hay que levantar el contenedor de la base de datos mediante docker compose con el siguiente comando
```bash
docker-compose up --build
```
Una vez terminado de instalar las dependencias y que se tiene arriba el contenedor de docker hay que ejecutar el siguiente comando para crear las tablas
```bash
npm run migrate
```
Ya que se terminaron de correr las migraciones ejecutamos el siguiente comando para insertar información en las tablas.
```bash
npm run seed
```

Para levantar el servidor se utiliza el siguiente comando
```bash
npm run start:dev
```

## Tecnologías utilizadas
* ReactJS
* NodeJS LTS
* Docker
* Docker compose
* Sequelize
