const matrizCaracol = (matriz) => {
    if (matriz.length === 0) {
        return [];
    }
    let resultado = [];
    let filaInicial = 0;
    let filaFinal = matriz.length - 1;
    let columnaInicial = 0;
    let columnaFinal = matriz[0].length - 1;

    while(true) {
        // Recorrido arriba
        for (let i = columnaInicial; i <= columnaFinal; i++) {
            resultado.push(matriz[filaInicial][i]);
        }
        filaInicial++;
        if (filaInicial > filaFinal) {
            return resultado;
        }

        // Recorrido por la derecha
        for (let i = filaInicial; i <= filaFinal; i++) {
            resultado.push(matriz[i][columnaFinal]);
        }
        columnaFinal--;
        if (columnaFinal < columnaInicial) {
            return resultado;
        }

        // Recorrido por abajo
        for (let i = columnaFinal; i >= columnaInicial; i--) {
            resultado.push(matriz[filaFinal][i]);
        }
        filaFinal--;
        if (filaFinal < filaInicial) {
            return resultado;
        }

        // Recorrido por la izquierda
        for (let i = filaFinal; i >= filaInicial; i--) {
            resultado.push(matriz[i][columnaInicial]);
        }
        columnaInicial++;
        if (columnaInicial > columnaFinal) {
            return resultado;
        }
    }
    return resultado;
};

console.log(matrizCaracol([[1,2,3],[4,5,6],[7,8,9]]));