import './App.css';
import { AppRouter } from './config/Layout';

function App() {
  return (
    <div>
      <AppRouter />
    </div>
  );
}

export default App;
