import { useState, useEffect } from "react"
import { Link } from "react-router-dom";
import { getBooksService } from "../../services/BooksService";

export const ItemListContainer = () => {
    const [books, setBooks] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        getBooksService().then(response => {
            setBooks(response.data.books);
        }).catch(error => {
            console.log(error);
        }).finally(() => {
            setLoading(false);
        });
    }, [])

    return (
        <>
            <div className="container">
                <h1>Listado de libros</h1>
                <table className="table is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Título</th>
                            <th>Autor</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {books.map((el) => {
                            return (
                                <tr key={el.id}>
                                    <td>{el.id}</td>
                                    <td>{el.title}</td>
                                    <td>{el.author}</td>
                                    <td>
                                        <Link to={`/app/users/${el.id}/edit`} className='button is-small is-info'>Detalle</Link>&nbsp;
                                        <button type='button' className="button is-small is-warning">Separar libro</button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </>
    )
}