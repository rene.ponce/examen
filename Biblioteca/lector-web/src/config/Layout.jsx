import {BrowserRouter, Routes, Route} from 'react-router-dom';
import { ItemListContainer } from '../components/ItemListContainer/ItemListContainer';
import { Navbar } from '../components/Navbar/Navbar';

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <Navbar />
            <Routes>
                <Route path='/' element={<ItemListContainer />} />
            </Routes>
        </BrowserRouter>
    )
};