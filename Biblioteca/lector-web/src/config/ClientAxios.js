import axios from 'axios';
const url = process.env.API || 'http://localhost:8000';
const token = localStorage.getItem('token') || '';
const clientAxios = axios.create({
    baseURL: url,
    headers: {
        'Authorization': token
    }
});

export default clientAxios;