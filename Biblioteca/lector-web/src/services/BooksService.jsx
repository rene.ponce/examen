import clientAxios from "../config/ClientAxios";

export const getBooksService = async () => {
    return await clientAxios.get('/public/books');
};

export const separateBook = async (data) => {
    return await clientAxios.post('/books/manage/separateBook',data)
};