import React, { useState } from 'react'
import {useNavigate} from 'react-router-dom';
import clientAxios from '../../config/ClientAxios';

const Login = () => {
    const navigate = useNavigate();
    const [values, setValues] = useState({
        email: '',
        password: ''
    });

    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    }

    const validateForm = () => {
        return values.email.length > 0 && values.password.length > 0;
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        clientAxios.post('/auth/login/admin', values).then(response => {
            const data = response.data;
            localStorage.setItem('token', data.token);
            if (response.status === 200) {
                navigate('app/main')
            }
        }).catch(error => {
            console.log(error);
        });
    }
  return (
    <div className='login-container'>
        <div className='card'>
            <div className='card-content'>
                <p className='title'>Login</p>
                <p className='subtitle'>Ingresa tus credenciales</p>
                <form onSubmit={handleSubmit}>
                    <div className='field'>
                        <label className='label'>Email</label>
                        <div className='control'>
                            <input className='input' name='email' type='email' placeholder='Email' value={values.email || ''} onChange={(e) => handleInputChange(e)} />
                        </div>
                    </div>
                    <div className='field'>
                        <label className='label'>Password</label>
                        <div className='control'>
                            <input className='input' name='password' type='password' placeholder='Password' value={values.password || ''} onChange={(e) => handleInputChange(e)} />
                        </div>
                    </div>
                    <button type='submit' className="button is-info is-fullwidth" disabled={!validateForm()}>Ingresar</button>
                </form>
            </div>
        </div>
    </div>
  )
}

export default Login;
