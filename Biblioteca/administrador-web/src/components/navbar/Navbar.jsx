import { Link, useNavigate } from 'react-router-dom';
import './Navbar.css';

const Navbar = ({sidebarOpen, openSidebar}) => {
  const navigate = useNavigate();

  const logOut = () => {
    localStorage.removeItem('token');
    navigate('/');
  }
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="https://bulma.io">
          <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28" />
        </a>

        <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-start">
          <Link to='/app/main' className="navbar-item">
            Inicio
          </Link>

          <Link to='/app/books' className="navbar-item">
            Libros
          </Link>

          <Link to='/app/users' className="navbar-item">
            Usuarios
          </Link>
        </div>
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              <button type='button' className="button is-primary" onClick={() => logOut()}>
                <strong>Cerrar Sesión</strong>
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar;