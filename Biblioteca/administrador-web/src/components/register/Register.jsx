import { useState } from "react";
import { useNavigate } from 'react-router-dom';
import clientAxios from "../../config/ClientAxios";

export const Register = () => {
    const navigate = useNavigate();
    const [values, setValues] = useState({
        name: '',
        email: '',
        password: '',
        role: ''
    });
    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    };
    const handleSubmit = () => {
        clientAxios.post('/auth/register/admin', values).then(response => {
            if (response.status === 201) {
                const data = response.data;
                localStorage.setItem('token', data.token);
                navigate('/app/main');
            }
        }).catch(error => {
            console.log(error);
        });
    }
    return (
        <>
            <div className='card'>
                <div className='card-content'>
                    <form method='post' onSubmit={handleSubmit}>
                        <div className="field">
                            <label className="label">Nombre</label>
                            <div className="control">
                                <input className="input" name='name' type="text" value={values.name} placeholder="Nombre" onChange={(e) => handleInputChange(e)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Email</label>
                            <div className="control">
                                <input className="input" name='email' type="email" value={values.email} placeholder="Email" onChange={(e) => handleInputChange(e)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Password</label>
                            <div className="control">
                                <input className="input" name='password' type="password" value={values.password} placeholder="Password" onChange={(e) => handleInputChange(e)} />
                            </div>
                        </div>
                        <button type='submit' className="button is-success">Registrar Administrador</button>&nbsp;
                    </form>
                </div>
            </div>
        </>
    )
}