import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import clientAxios from '../../config/ClientAxios';
import './ListBooks.css';

export const ListBooks = () => {
    const [books, setBooks] = useState([]);

    useEffect(() => {
        loadBooks();
    }, []);

    const loadBooks = () => {
        clientAxios.get('books').then(response => {
            setBooks(response.data.books);
        }).catch(error => {
            console.log(error);
        });
    };

    const deleteBook = (id) => {
        const r = window.confirm('¿Deseas eliminar el libro?');
        if (r) {
            clientAxios.delete(`books/${id}`).then((response) => {
                if (response.status === 200) {
                    loadBooks();
                }
            });
        }
    }
    
  return (
    <div>
        <div className='columns is-vcentered mt-10'>
            <div className='column is-10'>
                <h1>Listado de libros</h1>
            </div>
            <div className='colum is-2'>
                <Link to='/app/books/create' className='button is-link'>Agregar Libro</Link>
            </div>
        </div>
        <br />
        <table className='table is-fullwidth'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Autor</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {books.map((el) => {
                    return (
                        <tr key={el.id}>
                            <td>{el.id}</td>
                            <td>{el.title}</td>
                            <td>{el.author}</td>
                            <td>
                                <Link to={`/app/books/${el.id}/edit`} className='button is-small is-info'>Editar</Link>&nbsp;
                                <button type='button' className="button is-small is-danger" onClick={() => deleteBook(el.id)}>Borrar</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
  )
}
