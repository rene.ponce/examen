import React, {useEffect, useState} from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom';
import clientAxios from '../../config/ClientAxios';

export const FormBook = () => {
    const navigate = useNavigate();
    const {id} = useParams();
    const btnLabel = (id) ? 'Actualizar Libro' : 'Agregar Libro';
    const [values, setValues] = useState({
        title: '',
        author: '',
    });
    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    };
    const handleSubmit = (event) => {
        event.preventDefault();
        if (id !== undefined) {
            updateBook();
        } else {
            createBook();
        }
    };

    const createBook = () => {
        clientAxios.post('books', values).then(response => {
            if (response.status === 201) {
                navigate('/app/books');
            }
        }).catch(error => {
            console.log(error);
        });
    }

    const updateBook = () => {
        clientAxios.patch(`books/${id}`, values).then(response => {
            if (response.status === 202) {
                navigate('/app/books');
            }
        }).catch(error => {
            console.log(error);
        });
    }

    useEffect(() => {
      if (id) {
        clientAxios.get(`books/${id}`).then(response => {
            const data = response.data.book;
            setValues({
                title: data.title,
                author: data.author
            });
        }).catch(error => {
            console.log(error);
        });
      }
    }, []);
    
  return (
    <div className='card'>
        <div className='card-content'>
            <form method='post' onSubmit={handleSubmit}>
                <div className="field">
                    <label className="label">Título</label>
                    <div className="control">
                        <input className="input" name='title' type="text" value={values.title} placeholder="Título" onChange={(e) => handleInputChange(e)} />
                    </div>
                </div>
                <div className="field">
                    <label className="label">Autor</label>
                    <div className="control">
                        <input className="input" name='author' type="text" value={values.author} placeholder="Autor" onChange={(e) => handleInputChange(e)} />
                    </div>
                </div>
                <button type='submit' className="button is-success">{btnLabel}</button>&nbsp;
                <Link to='/app/books' className="button is-danger">Regresar</Link>
            </form>
        </div>
    </div>
  )
}
