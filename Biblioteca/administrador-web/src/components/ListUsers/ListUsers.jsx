import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import clientAxios from '../../config/ClientAxios';
import './ListUsers.css';

export const ListUsers = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        loadUsers();
    }, []);

    const loadUsers = () => {
        clientAxios.get('users').then(response => {
            setUsers(response.data.users);
        }).catch(error => {
            console.log(error);
        });
    }

    const deleteUser = (id) => {
        const r = window.confirm('¿Deseas eliminar el usuario?');
        if (r) {
            clientAxios.delete(`users/${id}`).then(response => {
                if (response.status === 200) {
                    loadUsers();
                }
            });
        }
    }
    
  return (
    <div>
        <div className='columns is-vcentered mt-10'>
            <div className='column is-10'>
                <h1>Listado de usuarios</h1>
            </div>
            <div className='colum is-2'>
                <Link to='/app/users/create' className='button is-link'>Agregar Usuario</Link>
            </div>
        </div>
        <br />
        <table className='table is-fullwidth'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {users.map((el) => {
                    return (
                        <tr key={el.id}>
                            <td>{el.id}</td>
                            <td>{el.name}</td>
                            <td>{el.email}</td>
                            <td>{el.role}</td>
                            <td>
                                <Link to={`/app/users/${el.id}/edit`} className='button is-small is-info'>Editar</Link>&nbsp;
                                <button type='button' className="button is-small is-danger" onClick={() => deleteUser(el.id)}>Borrar</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
  )
}
