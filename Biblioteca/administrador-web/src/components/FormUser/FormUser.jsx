import React, {useState, useEffect} from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom';
import clientAxios from '../../config/ClientAxios';

export const FormUser = () => {
    const navigate = useNavigate();
    const {id} = useParams();
    const btnLabel = (id) ? 'Actualizar Usuario' : 'Agregar Usuario';
    const [values, setValues] = useState({
        name: '',
        email: '',
        password: '',
        role: ''
    });
    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    };
    const handleSubmit = (event) => {
        event.preventDefault();
        if (id !== undefined) {
            updateUser();
        } else {
            createUser();
        }
    };

    const createUser = () => {
        clientAxios.post('users', values).then(response => {
            if (response.status === 201) {
                navigate('/app/users');
            }
        }).catch(error => {
            console.log(error);
        });
    }

    const updateUser = () => {
        clientAxios.patch(`users/${id}`, values).then(response => {
            if (response.status === 202) {
                navigate('/app/users');
            }
        }).catch(error => {
            console.log(error);
        });
    }

    useEffect(() => {
        if (id) {
          clientAxios.get(`users/${id}`).then(response => {
              const data = response.data.user;
              setValues({
                  name: data.name,
                  email: data.email,
                  password: data.password,
                  role: data.role,
              });
          }).catch(error => {
              console.log(error);
          });
        }
      }, []);
  return (
    <div className='card'>
        <div className='card-content'>
            <form method='post' onSubmit={handleSubmit}>
                <div className="field">
                    <label className="label">Nombre</label>
                    <div className="control">
                        <input className="input" name='name' type="text" value={values.name} placeholder="Nombre" onChange={(e) => handleInputChange(e)} />
                    </div>
                </div>
                <div className="field">
                    <label className="label">Email</label>
                    <div className="control">
                        <input className="input" name='email' type="email" value={values.email} placeholder="Email" onChange={(e) => handleInputChange(e)} />
                    </div>
                </div>
                <div className="field">
                    <label className="label">Password</label>
                    <div className="control">
                        <input className="input" name='password' type="password" value={values.password} placeholder="Password" onChange={(e) => handleInputChange(e)} />
                    </div>
                </div>
                <div className="field">
                    <label className="label">Roles</label>
                    <div className="control">
                        <div className="select">
                        <select name='role' value={values.role} onChange={(e) => handleInputChange(e)}>
                            <option value=''>Seleccione</option>
                            <option value='administrador'>Administrador</option>
                            <option value='bibliotecario'>Bibliotecario</option>
                            <option value='lector'>Lector</option>
                        </select>
                        </div>
                    </div>
                </div>
                <button type='submit' className="button is-success">{btnLabel}</button>&nbsp;
                <Link to='/app/users' className="button is-danger">Regresar</Link>
            </form>
        </div>
    </div>
  )
}
