import clientAxios from "../config/ClientAxios";

export const authUser = async (userData) => {
    return await clientAxios.post('/login', {userData});
}

export const saveLocal = (data) => {
    localStorage.setItem('token', data.token);
}