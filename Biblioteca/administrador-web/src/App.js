import { AppRouter } from './config/Layout';

const App = () => {
  
  return (
    <div className="container">
      <AppRouter />
    </div>
  );
}

export default App;
