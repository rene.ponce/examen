import {BrowserRouter, Outlet, Route, Routes} from 'react-router-dom';
import { FormUser } from '../components/FormUser/FormUser';
import { FormBook } from '../components/FormBook/FormBook';
import { ListBooks } from '../components/ListBooks/ListBooks';
import { ListUsers } from '../components/ListUsers/ListUsers';
import Login from '../components/login/Login';
import Main from '../components/main/Main';
import Navbar from '../components/navbar/Navbar';
import { Register } from '../components/register/Register';

const Layout = () => {
    return (
        <>
            <Navbar />
            <main className='container'>
                <Outlet />
            </main>
        </>
    )
}

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Login />} />
                <Route path='/register' element={<Register />} />
                <Route path='/app' element={<Layout />}>
                    <Route path='/app/main' element={<Main />} />
                    <Route path='/app/books' element={<ListBooks />} />
                    <Route path='/app/users' element={<ListUsers />} />
                    <Route path='/app/books/create' element={<FormBook />} />
                    <Route path='/app/books/:id/edit' element={<FormBook />} />
                    <Route path='/app/users/create' element={<FormUser />} />
                    <Route path='/app/users/:id/edit' element={<FormUser />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}
