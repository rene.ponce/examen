'use strict';
const bcryptjs = require('bcryptjs');
module.exports = {
  async up (queryInterface, Sequelize) {
   await queryInterface.bulkInsert('Users', [{
     name: 'Administrador',
     email: 'admin@biblioteca.com',
     password: bcryptjs.hashSync('admin', 10),
     role: 'administrador',
     createdAt: new Date(),
     updatedAt: new Date()
   }], {});
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('Users', null, {});
  }
};
