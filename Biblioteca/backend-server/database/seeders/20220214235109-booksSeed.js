'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Books', [
     {
       title: 'Como programar en java',
       author: 'Deitel & Deitel',
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
      title: 'Algebra',
      author: 'Baldor',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'Libro de prueba',
      author: 'Jhon Doe',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'Mi libro de ayuda',
      author: 'Juan Pérez',
      createdAt: new Date(),
      updatedAt: new Date()
    }
   ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Books', null, {});
  }
};
