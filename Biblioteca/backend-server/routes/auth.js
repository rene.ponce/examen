import { Router } from 'express';
import { loginAdminUser, registerAdminUser } from '../controllers/auth';

const router = Router();

router.post('/login/admin', loginAdminUser);
router.post('/register/admin', registerAdminUser)

export default router;