import { Router } from 'express';
import { separateBook } from '../controllers/manageBook';
import auth from '../middleware/auth';

const router = Router();

router.post('/separateBook', auth, separateBook);

export default router;