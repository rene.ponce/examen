import { Router } from 'express';
import {getUsers, getUser, createUser, updateUser, deleteUser} from '../controllers/user';
import auth from '../middleware/auth';


const router = Router();

// Get all users
router.get('/', auth, getUsers);

// Get user by id
router.get('/:id', auth, getUser);

// Create user
router.post('/', auth, createUser);

// Update user
router.patch('/:id', auth, updateUser);

// Delete user
router.delete('/:id', auth, deleteUser);

export default router;