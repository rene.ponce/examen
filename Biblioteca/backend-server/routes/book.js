import { Router } from 'express';
import {createBook, deleteBook, getBook, getBooks, updateBook} from '../controllers/book';
import auth from '../middleware/auth';


const router = Router();

// Get all books
router.get('/', auth, getBooks);

// Get book by id
router.get('/:id', auth, getBook);

// Create book
router.post('/', auth, createBook);

// Update book
router.patch('/:id', auth, updateBook);

// Delete book
router.delete('/:id', auth, deleteBook);

export default router;