import {Router} from 'express';
import {getBook, getBooks} from '../controllers/book';

const router = Router();

// Get books
router.get('/books', getBooks);

// Get single book
router.get('/books/:id', getBook);

export default router;