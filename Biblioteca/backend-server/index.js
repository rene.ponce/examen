require('dotenv').config();
import express from 'express';
import cors from 'cors';
import publicRoutes from './routes/public';
import userRoutes from './routes/user';
import authRoutes from './routes/auth';
import bookRoutes from './routes/book';
import manageBooksRoutes from './routes/manageBook';


// Variables
const app = express();
const port = process.env.PORT || 5000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors());

// Public Routes
app.use('/public', publicRoutes);

// Routes
app.use('/users', userRoutes);
app.use('/auth', authRoutes);
app.use('/books', bookRoutes);
app.use('/books/manage', manageBooksRoutes);


// Version route
app.get('/', (req, res) => {
    res.status(200).send({message: 'Api'})
});

// Start server
app.listen(port, () => console.log(`Server is running on PORT ${port}`));