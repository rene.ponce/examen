const User = require('../database/models').User;
import bcryptjs from 'bcryptjs';

const getUsers = async (req, res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const getUser = async (req, res) => {
    try {
        const {id} = req.params;
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const createUser = async (req, res) => {
    try {
        const {name, email, password, role} = req.body;
        User.create({
            name,
            email,
            password: bcryptjs.hashSync(password, 10),
            role
        }).then((user) => {
            return res.status(201).json({user});
        }).catch(err => {
            return res.status(400).message({err});
        });
    } catch (error) {
        
    }
};

const updateUser = async (req, res) => {
    try {
        const {id} = req.params;
        const {name, email, password, role} = req.body;

        User.findOne({
            where: {id}
        }).then(user => {
            if (user) {
                user.update({
                    name,
                    email,
                    password: bcryptjs.hashSync(password, 10),
                    role
                }).then((updatedUser) => {
                    return res.status(202).json({updatedUser});
                });
            } else {
                return res.status(206).json({message: 'Usuario no encontrado'});
            }
        })
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const deleteUser = async (req, res) => {
    try {
        const {id} = req.params;
        User.destroy({
            where: {id}
        }).then(() => {
            return res.status(200).json({message: 'Usuario eliminado correctamente'});
        }).catch((error) => {
            return res.status(400).json({message: error.message});
        });
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

module.exports = {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
};