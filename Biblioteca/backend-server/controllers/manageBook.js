const SeparateBook = require('../database/models').SeparateBook;

const separateBook = async (req, res) => {
    try {
        const {bookId} = req.body;
        const userId = req.user.userId;
        SeparateBook.create({
            bookId,
            userId,
            status: 'en proceso'
        }).then(separate => {
            return res.status(201).json({separate});
        }).catch(error => {
            return res.status(400).json({message: error});
        });
    } catch (error) {
        return res.status(400).json({message: error.message});
    }
};

module.exports = {
    separateBook
}