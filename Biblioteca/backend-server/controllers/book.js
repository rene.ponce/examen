const Book = require('../database/models').Book;

const getBooks = async (req, res) => {
    try {
        const books = await Book.findAll();
        return res.status(200).json({books});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const getBook = async (req, res) => {
    try {
        const {id} = req.params;
        const book = await Book.findByPk(id);
        return res.status(200).json({book});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const createBook = async (req, res) => {
    try {
        const {title, author} = req.body;
        Book.create({
            title,
            author
        }).then(book => {
            return res.status(201).json({book});
        }).catch(error => {
            return res.status(400).json({message: error});
        });
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const updateBook = async (req, res) => {
    try {
        const {id} = req.params;
        const {title, author} = req.body;

        Book.findOne({
            where: {id}
        }).then(book => {
            if (book) {
                book.update({
                    title,
                    author
                }).then(updatedBook => {
                    return res.status(202).json({updatedBook});
                });
            } else {
                return res.status(206).json({message: 'Libro no encontrado'});
            }
        }).catch(error => {});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const deleteBook = async (req, res) => {
    try {
        const {id} = req.params;
        Book.destroy({
            where: {id}
        }).then(() => {
            return res.status(200).json({message: 'Libro eliminado correctamente'});
        }).catch(error => {
            return res.status(400).json({message: error.message});
        });
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

module.exports = {
    getBooks,
    getBook,
    createBook,
    updateBook,
    deleteBook
}