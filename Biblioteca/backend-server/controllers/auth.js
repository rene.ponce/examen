const User = require('../database/models').User;
import bcryptjs from 'bcryptjs';
import jsonwebtoken from 'jsonwebtoken';

const loginAdminUser = async (req, res) => {
    try {
        const {email, password} = req.body;

        if (!(email && password)) {
            return res.status(400).json({message: 'Todos los campos son necesarios'});
        }

        const user = await User.findOne({
            where: {
                email,
                role: 'administrador'
            }
        });

        if (user && (await bcryptjs.compare(password, user.password))) {
            const token = jsonwebtoken.sign(
                {userId: user.id, email},
                process.env.TOKEN_KEY || '35T335UNT0K3N3216548*/',
                {expiresIn: '8h'}
            );

            return res.status(200).json({user, token});
        }
        return res.status(400).json({message: 'Credenciales inválidas'});
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

const registerAdminUser = async (req, res) => {
    try {
        const {name, email, password} = req.body;
        User.create({
            name,
            email,
            password: bcryptjs.hashSync(password, 10),
            role: 'administrador'
        }).then((user) => {
            const token = jsonwebtoken.sign(
                {userId: user.id, email},
                process.env.TOKEN_KEY || '35T335UNT0K3N3216548*/',
                {expiresIn: '8h'}
            );
            return res.status(201).json({user, token});
        }).catch(err => {
            return res.status(400).message({err});
        });
    } catch (error) {
        
    }
};

module.exports = {
    loginAdminUser,
    registerAdminUser
}