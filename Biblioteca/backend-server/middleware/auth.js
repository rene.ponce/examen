import jsonwebtoken from 'jsonwebtoken';

const verifyToken = (req, res, next) => {
    const token = req.headers["authorization"];
    
    if (!token) {
        res.status(403).json({message: 'Se requiere token de autenticación'});
    }
    try {
        const key = process.env.TOKEN_KEY || '35T335UNT0K3N3216548*/';
        const decoded = jsonwebtoken.verify(token, key);
        req.user = decoded;
    } catch (error) {
        return res.status(401).json({message: 'Token inválido'});
    }
    return next();
};

export default verifyToken;